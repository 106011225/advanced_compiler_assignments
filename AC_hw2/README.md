# Advanced compiler assignments: Pointer analysis with LLVM passes


## Goal 

- To implement the pointer analysis technique described in the paper: *Guarna, Vincent A.. “A Technique for Analyzing Pointer and Structure References In Parallel Restructuring Compilers.” ICPP (1988)*
- Data flow analysis


Example source code:
```c
#include <stdio.h>

void icpp() 
{
  int x, y, *p, **pp;
  p = &x;
  pp = &p;
  *pp = &y;
  *p = 3;
}
```

Example output:
```
S1-------------------------
TREF: {}
TGEN: {p}
DEP: {}
TDEF: {(p, S1)}
TEQU: {(*p, x)}

S2-------------------------
TREF: {}
TGEN: {pp}
DEP: {}
TDEF: {(p, S1), (pp, S2)}
TEQU: {(**pp, *p), (**pp, x), (*p, x), (*pp, p)}

S3-------------------------
TREF: {pp}
TGEN: {*pp,p}
DEP: {
pp :S2 -------> S3
p :S1 ---O---> S3
}
TDEF: {(*pp, S3), (p, S3), (pp, S2)}
TEQU: {(**pp, *p), (**pp, y), (*p, y), (*pp, p)}

S4-------------------------
TREF: {*pp,p}
TGEN: {**pp,*p,y}
DEP: {
*pp :S3 -------> S4
p :S3 -------> S4
}
TDEF: {(**pp, S4), (*p, S4), (*pp, S3), (p, S3), (pp, S2), (y, S4)}
TEQU: {(**pp, *p), (**pp, y), (*p, y), (*pp, p)}

```

## Usage

Note: This project is tested under **LLVM9**.   

1. Compile the source code
    ```
    $ make
    ```

2. Test the results
    ```
    $ opt -load ./hw2.so -enable-new-pm=0 -hw2 icpp.ll > /dev/null
    $ opt -load ./hw2.so -enable-new-pm=0 -hw2 icpp2.ll > /dev/null
    $ opt -load ./hw2.so -enable-new-pm=0 -hw2 icpp3.ll > /dev/null
    $ opt -load ./hw2.so -enable-new-pm=0 -hw2 foo.ll > /dev/null
    ```

**Please refer to [SPEC.pdf](./SPEC.pdf) and [report.pdf](./report.pdf) for more details.**
