#include <llvm/Pass.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/IntrinsicInst.h>
#include <llvm/IR/DebugInfoMetadata.h>
#include <llvm/Support/raw_ostream.h>

#include <llvm/Analysis/ScalarEvolution.h>

// #include <llvm/PassAnalysisSupport.h> // include @ LLVM9
#include <llvm/Pass.h> // include @ LLVM14

#include <llvm/ADT/Statistic.h>
#include <vector>
#include <map>
#include <set>


namespace {

    int FULL_TRANSITIVE_CLOSURE = 1;
    int PRINT_TREF_ALIAS = 1;

    typedef std::set<std::pair<std::string, unsigned int>> IOGK_SET;
    typedef std::set<std::pair<std::string, std::string>> EQ_SET;


    struct HW2Pass : public llvm::FunctionPass{
        static char ID;
        HW2Pass() : FunctionPass(ID) {}

        std::map<llvm::Value*, std::string> VarNameDict;
        unsigned int curr_stmt = 1;

        void get_starTree_and_insert(IOGK_SET &Tset, llvm::Instruction* I, char LR, std::string mode){
            /* 
             * get star-tree (dereference subtree) and insert it to Tset; assert: I is a LoadInst
             * LR: 'L' or 'R', LHS or RHS
             * mode: "single", "subtree", "properSubtree"
             */

            if (LR == 'R') I = llvm::dyn_cast<llvm::Instruction>(I->getOperand(0));

            if (mode == "single"){
                std::string tree_str = "";

                while (I->getOpcode() == llvm::Instruction::Load) {
                    tree_str += "*";
                    I = llvm::dyn_cast<llvm::Instruction>(I->getOperand(0));
                }

                tree_str += VarNameDict[I]; // assert now I is a AllocaInst
                std::pair<std::string, unsigned int> element = {tree_str, curr_stmt};
                Tset.insert(element);
            }
            else { // mode == properSubtree or subtree

                if (mode == "properSubtree") {
                    if (I->getOpcode() != llvm::Instruction::Load) return; // no properSubtree
                    I = llvm::dyn_cast<llvm::Instruction>(I->getOperand(0));
                }

                int cnt = 0;
                while (I->getOpcode() == llvm::Instruction::Load) {
                    cnt++;
                    I = llvm::dyn_cast<llvm::Instruction>(I->getOperand(0));
                }

                std::string tree_str = VarNameDict[I]; // assert now I is a AllocaInst
                for (; cnt >= 0; cnt--, tree_str="*"+tree_str){
                    std::pair<std::string, unsigned int> element = {tree_str, curr_stmt};
                    Tset.insert(element);
                }
            }
        } // end of function definition

        void add_equiv(IOGK_SET &Tset, EQ_SET EQset){
            IOGK_SET temp_SET = Tset;
            for (auto x : temp_SET){
                for (auto eqPair : EQset){
                    std::string str1 = eqPair.first;
                    std::string str2 = eqPair.second;
                    if (x.first == str1) {
                        auto element = x;
                        element.first = str2;
                        Tset.insert(element);
                    }
                    else if (x.first == str2){
                        auto element = x;
                        element.first = str1;
                        Tset.insert(element);
                    }
                }
            }
        }

        void find_REF(IOGK_SET &TREF, llvm::Value *tree, char LR){
            if (llvm::Instruction* I = llvm::dyn_cast<llvm::Instruction>(tree)){ // if tree != constant
                // we only insert maximum components
                if (LR == 'R'){ // RHS
                    if (I->getOpcode() == llvm::Instruction::Load){
                        get_starTree_and_insert(TREF, I, 'R', "subtree");
                    }
                    else if (I->getOpcode() == llvm::Instruction::Add){
                        find_REF(TREF, I->getOperand(0), 'R');
                        find_REF(TREF, I->getOperand(1), 'R');
                    }
                    // if RHS is an AllocaInst, then it's a &x
                }
                else if (LR == 'L'){ // LHS
                    if (I->getOpcode() == llvm::Instruction::Load){
                        get_starTree_and_insert(TREF, I, 'L', "properSubtree");
                    }
                }
            }
        }

        void find_GEN(IOGK_SET &TGEN, llvm::Value *tree, char LR){
            if (llvm::Instruction* I = llvm::dyn_cast<llvm::Instruction>(tree)){ // if tree != constant
                if (LR == 'L'){
                    get_starTree_and_insert(TGEN, I, 'L', "single");
                }
            }
        }

        void print_dependence(IOGK_SET TIN, IOGK_SET TREF, IOGK_SET TGEN){
            llvm::errs() << "DEP: {";
            int notfound = 1;

            // flow dependencies
            for (auto x : TIN){
                for (auto r : TREF){
                    if (x.first == r.first){
                        notfound = 0;
                        llvm::errs() << "\n" << x.first << " :S" << x.second << " -------> S" << r.second;
                    }
                }
            }

            // output dependencies
            for (auto x : TIN){
                for (auto g : TGEN){
                    if (x.first == g.first){
                        notfound = 0;
                        llvm::errs() << "\n" << x.first << " :S" << x.second << " ---O---> S" << g.second;
                    }
                }
            }

            llvm::errs() << (notfound ? "}\n" : "\n}\n");
        }

        void cal_KILL(IOGK_SET &TKILL, IOGK_SET TIN, IOGK_SET TGEN){
            int addToKill;
            for (auto x : TIN){
                addToKill = 0;
                std::string tree_str = x.first;
                do {
                    for (auto g : TGEN){
                        if (tree_str == g.first){
                            addToKill = 1;
                            break;
                        }
                    }
                    if (addToKill){
                        TKILL.insert(x);
                        break;
                    }

                    if (tree_str[0] != '*') break;
                    else tree_str.erase(tree_str.begin());
                } while(!tree_str.empty());
            }
        }

        void cal_OUT(IOGK_SET &TOUT, IOGK_SET TGEN, IOGK_SET TIN, IOGK_SET TKILL){
            TOUT = TIN;
            for (auto x : TKILL) TOUT.erase(x);
            for (auto x : TGEN) TOUT.insert(x);
        }

        void insert_equiv_pair(EQ_SET &EQset, std::string str1, std::string str2){
            if (str1 < str2){
                std::pair<std::string, std::string> element = {str1, str2};
                EQset.insert(element);
            }
            else if (str1 > str2){
                std::pair<std::string, std::string> element = {str2, str1};
                EQset.insert(element);
            }
        }

        void cal_EQ_KILL(EQ_SET &EQuiv_KILL, EQ_SET EQuiv_IN, IOGK_SET TGEN){
            int addToKill;
            for (auto x : EQuiv_IN){
                addToKill = 0;
                std::string tree_str1 = x.first;
                std::string tree_str2 = x.second;
                
                while(!addToKill && tree_str1[0] == '*'){
                    tree_str1.erase(tree_str1.begin());
                    for (auto g : TGEN){
                        if (tree_str1 == g.first){
                            addToKill = 1;
                            break;
                        }
                    }
                }
                while(!addToKill && tree_str2[0] == '*'){
                    tree_str2.erase(tree_str2.begin());
                    for (auto g : TGEN){
                        if (tree_str2 == g.first){
                            addToKill = 1;
                            break;
                        }
                    }
                }
                
                if (addToKill) EQuiv_KILL.insert(x);
            }
        }

        void cal_EQ_GEN(EQ_SET &EQuiv_GEN, llvm::Value* LHS, llvm::Value* RHS){
            llvm::Instruction *I_lhs = llvm::dyn_cast<llvm::Instruction>(LHS);
            llvm::Instruction *I_rhs = llvm::dyn_cast<llvm::Instruction>(RHS);
            if (I_lhs && I_rhs && I_lhs->getType()->getPointerElementType()->isPointerTy()){ // exist && LHS results in a pointer

                std::string str_lhs = "*";
                while(I_lhs->getOpcode() != llvm::Instruction::Alloca){
                    str_lhs += "*";
                    I_lhs = llvm::dyn_cast<llvm::Instruction>(I_lhs->getOperand(0));
                }
                str_lhs += VarNameDict[I_lhs];

                std::string str_rhs = "";
                while(I_rhs->getOpcode() != llvm::Instruction::Alloca){
                    str_rhs += "*";
                    I_rhs = llvm::dyn_cast<llvm::Instruction>(I_rhs->getOperand(0));
                }
                str_rhs += VarNameDict[I_rhs];

                insert_equiv_pair(EQuiv_GEN, str_lhs, str_rhs);
            }
        }

        void cal_EQ_OUT(EQ_SET &EQuiv_OUT, EQ_SET EQuiv_GEN, EQ_SET EQuiv_IN, EQ_SET EQuiv_KILL){
            EQuiv_OUT = EQuiv_IN;
            for (auto x : EQuiv_KILL) EQuiv_OUT.erase(x);
            for (auto x : EQuiv_GEN) EQuiv_OUT.insert(x);

            // transitive closure 
            int changed = 1;
            while(changed){
                changed = 0;
                EQ_SET temp_SET = EQuiv_OUT;

                for (auto x : EQuiv_OUT){ // for each element
                    std::string str1 = x.first;
                    std::string str2 = x.second;
                    for (int i = 0; i <= 1; i++, str1="*"+str1, str2="*"+str2){ // also check one more dereference level
                        if ((!FULL_TRANSITIVE_CLOSURE) && i == 0) continue;
                        for (auto eqPair : EQuiv_OUT){
                            if (eqPair == x) continue;
                            if (str1 == eqPair.first) { // (a, b), (a, c)
                                insert_equiv_pair(temp_SET, str2, eqPair.second);
                            }
                            else if (str1 == eqPair.second){ // (b, c), (a, b)
                                insert_equiv_pair(temp_SET, str2, eqPair.first);
                            }
                            else if (str2 == eqPair.first) { // (a, b), (b, c)
                                insert_equiv_pair(temp_SET, str1, eqPair.second);
                            }
                            else if (str2 == eqPair.second){ // (a, c), (b, c)
                                insert_equiv_pair(temp_SET, str1, eqPair.first);
                            }
                        }
                    }
                }// end of for each element
                
                if (temp_SET.size() > EQuiv_OUT.size()){
                    EQuiv_OUT = temp_SET;
                    changed = 1;
                }
            } // end of while(changed)
        }

        void print_TSET(IOGK_SET TSET, std::string set_name, int verbose) {
            if (!verbose){
                llvm::errs() << set_name << ": {";
                if (!TSET.empty()) {
                    auto it = TSET.begin();
                    llvm::errs() << it->first;
                    it++;
                    for (; it != TSET.end(); it++)
                        llvm::errs() << "," << it->first;
                }
                llvm::errs() << "}\n";
            }
            else { 
                llvm::errs() << set_name << ": {";
                if (!TSET.empty()) {
                    auto it = TSET.begin();
                    llvm::errs() << "(" << it->first << ", S" << it->second << ")";
                    it++;
                    for (; it != TSET.end(); it++)
                        llvm::errs() << ", (" << it->first << ", S" << it->second << ")";
                }
                llvm::errs() << "}\n";
            }
        }

        void print_EQ(EQ_SET EQuiv_IN){
            llvm::errs() << "TEQU: {";
            if (!EQuiv_IN.empty()) {
                auto it = EQuiv_IN.begin();
                llvm::errs() << "(" << it->first << ", " << it->second << ")";
                it++;
                for (; it != EQuiv_IN.end(); it++)
                    llvm::errs() << ", (" << it->first << ", " << it->second << ")";
            }
            llvm::errs() << "}\n";
        }

        virtual bool runOnFunction(llvm::Function &F) {
            
            // construct VarNameDict, key: AllocaInst, value: variable name
            for (llvm::Function::iterator BB = F.begin(); BB != F.end(); BB++){
                for (llvm::BasicBlock::iterator I = BB->begin(); I != BB->end(); I++){
                    if (const llvm::DbgDeclareInst *DI = llvm::dyn_cast<llvm::DbgDeclareInst>(I)){
                        llvm::Value *VarAllocInst = DI->getAddress();
                        std::string VarName = DI->getVariable()->getName().str();
                        VarNameDict[VarAllocInst] = VarName;
                    }
                }
            }

            // for each StoreInst (assignment stmt), generate info.
            IOGK_SET TIN;
            IOGK_SET TOUT;
            EQ_SET EQuiv_IN;
            EQ_SET EQuiv_OUT;
            for (llvm::Function::iterator BB = F.begin(); BB != F.end(); BB++){
                for (llvm::BasicBlock::iterator I = BB->begin(); I != BB->end(); I++){
                    if (const llvm::StoreInst *SI = llvm::dyn_cast<llvm::StoreInst>(I)){

                        llvm::errs() << "S" << curr_stmt << "-------------------------\n";
                        TIN = TOUT;
                        EQuiv_IN = EQuiv_OUT;
                    
                        IOGK_SET TREF;
                        IOGK_SET TGEN;
                        IOGK_SET TKILL;
                        EQ_SET EQuiv_GEN;
                        EQ_SET EQuiv_KILL;

                        llvm::Value *LHS = SI->getOperand(1);
                        llvm::Value *RHS = SI->getOperand(0);

                        find_REF(TREF, RHS, 'R');
                        find_REF(TREF, LHS, 'L');
                        if (!PRINT_TREF_ALIAS) print_TSET(TREF, "TREF", 0);
                        add_equiv(TREF, EQuiv_IN);
                        if (PRINT_TREF_ALIAS) print_TSET(TREF, "TREF", 0);
                        
                        find_GEN(TGEN, LHS, 'L');
                        add_equiv(TGEN, EQuiv_IN);
                        print_TSET(TGEN, "TGEN", 0);

                        print_dependence(TIN, TREF, TGEN); 

                        cal_KILL(TKILL, TIN, TGEN);
                        cal_OUT(TOUT, TGEN, TIN, TKILL);
                        print_TSET(TOUT, "TDEF", 1);

                        cal_EQ_KILL(EQuiv_KILL, EQuiv_IN, TGEN);
                        cal_EQ_GEN(EQuiv_GEN, LHS, RHS);
                        cal_EQ_OUT(EQuiv_OUT, EQuiv_GEN, EQuiv_IN, EQuiv_KILL);
                        print_EQ(EQuiv_OUT);

                        llvm::errs() << "\n";
                        curr_stmt++;
                    } // if Inst is a StoreInst
                } // for each Inst
            } // for each BB

            return false;
        } // end of runOnFunction
    }; // end of struct HW2Pass
} // end of namespace

char HW2Pass::ID = 0;

//register class
static llvm::RegisterPass<HW2Pass> X("hw2", "hw2 PASS, Reaching Definition and Flow Equations");
