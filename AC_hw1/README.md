# Advanced compiler assignments: Data dependence analysis with LLVM passes


## Goal

- Using the LLVM intermediate form to do data dependence analysis for C programs with for loops. 
- **Report the dependence information in the for-loop**. 
- The data dependence in this project **only concerns about the dependences between array variables**. 


#### Case 1

We consider `i + c` style of array index as follows. To consider one-dimensional arrays and a single level loop. The format `A[f(i)]`, where `f(i)` is `i + c`

```c
//test1.c
for (i=0; i<20; i++) {
  A[i] = C[i];
  D[i] = A[i-4];
}
```

#### Case 2

We also consider `c * i + d` style of array index as follows. To consider one-dimensional arrays and a single level loop. The format `A[f(i)]`, where `f(i)` is `c * i + d`

```c
//test2.c
for (i=0; i<20; i++) {
  A[i] = C[i];
  D[i] = A[3*i-4];
	D[i-1] = C[2*i];
}
```


## Usage

Note: This project is tested under **LLVM9**.

1. Compile the source code
    ```
    $ make
    ```

2. Test the results
    ```
    $ opt -load ./hw1.so -enable-new-pm=0 -hw1 test1.ll > /dev/null
    $ opt -load ./hw1.so -enable-new-pm=0 -hw1 test2.ll > /dev/null
    $ opt -load ./hw1.so -enable-new-pm=0 -hw1 test3.ll > /dev/null
    ```

**Please refer to [SPEC.pdf](./SPEC.pdf) and [report.pdf](./report.pdf) for more details.**
