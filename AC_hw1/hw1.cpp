#include <llvm/Pass.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/IntrinsicInst.h>
#include <llvm/IR/DebugInfoMetadata.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Analysis/LoopPass.h>
#include <llvm/Analysis/LoopInfo.h>

#include <llvm/Analysis/ScalarEvolution.h>

// #include <llvm/PassAnalysisSupport.h> // include @ LLVM9
#include <llvm/Pass.h> // include @ LLVM14

#include <llvm/ADT/Statistic.h>
#include <vector>
#include <map>


namespace {

    struct ArrInfo {
        char type; // 'U':use  /  'D': def

        std::string arrName; // array name
        unsigned int lineNum; // @ which line in the source code
        unsigned int columnNum; // @ which column in the source code
        unsigned int statement_th; // is ?th statement

        std::string iName; // index name
        int64_t iMultiplier; // ? * i
        int64_t iAddend; // i + ?
        int64_t i_start; // i : range(i_start, i_stop, i_step)
        int64_t i_stop;
        int64_t i_step;
    };

    struct ArrAccessInfo {
        char type; // 'U':use  /  'D':def
        int statement_th; // is ?th statement
        std::string iName;
        int i; // arr[ci + b], which i?
    };


    struct HW1Pass : public llvm::LoopPass{
        static char ID;
        HW1Pass() : LoopPass(ID) {}

        int64_t find_i_start(llvm::AllocaInst *AI_i, llvm::BasicBlock *Preheader){
            int64_t retVal;
            for (llvm::BasicBlock::iterator I = Preheader->begin(); I != Preheader->end(); I++){
                if (I->getOpcode() == llvm::Instruction::Store && // is a def
                        I->getOperand(1) == AI_i){ // && def of i
                    llvm::ConstantInt *C = llvm::dyn_cast<llvm::ConstantInt>(I->getOperand(0));
                    retVal = C->getSExtValue();
                }
            }  
            return retVal;
        }

        int64_t find_i_stop(llvm::AllocaInst *AI_i, llvm::BasicBlock *Exiting){
            int64_t retVal;
            llvm::ICmpInst *ICI = llvm::dyn_cast<llvm::ICmpInst>(Exiting->getTerminator()->getOperand(0));
            llvm::ConstantInt *C = llvm::dyn_cast<llvm::ConstantInt>(ICI->getOperand(1));

            retVal = C->getSExtValue(); 
            if (ICI->isTrueWhenEqual()) retVal += 1; // if op is "<=" rather than "<", add 1 (assume i is increasing)
            return retVal; 
        }

        int64_t find_i_step(llvm::AllocaInst *AI_i, llvm::BasicBlock *Latch){

            int64_t retVal;
            for (llvm::BasicBlock::iterator I = Latch->begin(); I != Latch->end(); I++){
                if (I->getOpcode() == llvm::Instruction::Store && // if it is a def
                        I->getOperand(1) == AI_i){ // && def of i
                    llvm::Instruction *oneStepInst = llvm::dyn_cast<llvm::Instruction>(I->getOperand(0));

                    // assume `oneStepInst` is an add instr
                    llvm::ConstantInt *C = llvm::dyn_cast<llvm::ConstantInt>(oneStepInst->getOperand(1));
                    retVal = C->getSExtValue();
                    break;
                }
            }
            return retVal;
        }

        void report_dependence(const std::map<std::string, std::vector<std::vector<ArrAccessInfo>>> EachArrTable, std::string dependence_type){

            if (dependence_type == "flow") llvm::errs() << "====Flow Dependency====\n";
            else if (dependence_type == "anti") llvm::errs() << "====Anti-Dependency====\n";
            else if (dependence_type == "output") llvm::errs() << "====Output Dependency====\n";

            int notfound = 1;
            for (auto x : EachArrTable){
                std::vector<std::vector<ArrAccessInfo>> table = x.second;

                for (int64_t index=0; index < table.size(); index++){
                    std::vector<ArrAccessInfo> arrElementAccessList = table.at(index);
                    if (arrElementAccessList.size() > 1) { // an element of an array has been accessed more than one time


                        // print out vector contents
                        // llvm::errs() << x.first << "[" << index << "]: ";
                        // for (ArrAccessInfo access : arrElementAccessList) {
                        //     llvm::errs() << "S" << access.statement_th << ":"
                        //                  << access.type << "@" << access.iName << "=" << access.i << " ";
                        // }
                        // llvm::errs() << "\n";

                        if (dependence_type == "flow"){
                            std::vector<ArrAccessInfo>::iterator it = arrElementAccessList.begin();

                            while(it != arrElementAccessList.end() && it->type != 'D') it++;
                            if (it == arrElementAccessList.end()) continue; // No D-U chain
                            ArrAccessInfo prev_D = *it;

                            for (it++ ; it != arrElementAccessList.end(); it++){
                                if (it->type == 'D') prev_D = *it;
                                else { // type == 'U', find a D-U chain
                                    notfound = 0;
                                    llvm::errs() << "(" << prev_D.iName << "=" << prev_D.i << "," << it->iName << "=" << it->i << ") "
                                        << x.first << ": " 
                                        << "S" << prev_D.statement_th << " ---> " << "S" << it->statement_th 
                                        << "  (at " << x.first << "[" << index << "])\n";
                                }
                            }
                        }
                        else if (dependence_type == "anti"){
                            std::vector<ArrAccessInfo>::iterator it = arrElementAccessList.begin();

                            while(it != arrElementAccessList.end() && it->type != 'U') it++;
                            if (it == arrElementAccessList.end()) continue; // No U-D chain
                            ArrAccessInfo prev_U = *it;

                            for (it++ ; it != arrElementAccessList.end(); it++){
                                if (it->type == 'U') prev_U = *it;
                                else { // type == 'D', find a U-D chain
                                    notfound = 0;
                                    llvm::errs() << "(" << prev_U.iName << "=" << prev_U.i << "," << it->iName << "=" << it->i << ") " 
                                        << x.first << ": "
                                        << "S" << prev_U.statement_th << " ---> " << "S" << it->statement_th 
                                        << "  (at " << x.first << "[" << index << "])\n";
                                }
                            }
                        }
                        else if (dependence_type == "output"){
                            std::vector<ArrAccessInfo>::iterator it = arrElementAccessList.begin();

                            while(it != arrElementAccessList.end() && it->type != 'D') it++;
                            if (it == arrElementAccessList.end()) continue; // No D-D chain
                            ArrAccessInfo prev_D = *it;

                            for (it++ ; it != arrElementAccessList.end(); it++){
                                if (it->type == 'D'){ // find a D-D chain
                                    notfound = 0;
                                    llvm::errs() << "(" << prev_D.iName << "=" << prev_D.i << "," << it->iName << "=" << it->i << ") "
                                        << x.first << ": "
                                        << "S" << prev_D.statement_th << " ---> " << "S" << it->statement_th 
                                        << "  (at " << x.first << "[" << index << "])\n";
                                    prev_D = *it;
                                } 
                            }
                        }
                    } // end of if "an element of an array has been accessed more than one time"
                } // end of a table
            } // end of for each table
            if (notfound) llvm::errs() << "(not found)\n";
        } // end of function definition


        virtual bool runOnLoop(llvm::Loop *L, llvm::LPPassManager &LPM) {
            std::vector<ArrInfo> ArrAccessList;
            std::map<llvm::Value *, std::string> VarNameDict;
            unsigned int curr_statement = 1;
            unsigned int arrayMaxSize = 1;

            // construct VarNameDict
            llvm::Module *M = L->getHeader()->getModule();
            for (llvm::Module::iterator F = M->begin(); F != M->end(); F++) {
                for (llvm::Function::iterator BB = F->begin(); BB != F->end(); BB++){
                    for (llvm::BasicBlock::iterator I = BB->begin(); I != BB->end(); I++){
                        if (const llvm::DbgDeclareInst *DI = llvm::dyn_cast<llvm::DbgDeclareInst>(I)){
                            llvm::Value *VarAllocInst = DI->getAddress();
                            llvm::StringRef VarName = DI->getVariable()->getName().str();
                            VarNameDict[VarAllocInst] = VarName;
                        }
                    }
                }
            }

            // construct ArrAccessList
            for (llvm::BasicBlock *BB : L->getBlocks()){
                for (llvm::BasicBlock::iterator I = BB->begin(); I != BB->end(); I++) {

                    llvm::Instruction *LSTarget = NULL;
                    ArrInfo record;
                    if (const llvm::StoreInst *SI = llvm::dyn_cast<llvm::StoreInst>(I)){
                        LSTarget = llvm::dyn_cast<llvm::Instruction>(SI->getOperand(1));
                        record.type = 'D';
                        record.statement_th = curr_statement++;
                    }
                    else if (const llvm::LoadInst *LI = llvm::dyn_cast<llvm::LoadInst>(I)){
                        LSTarget = llvm::dyn_cast<llvm::Instruction>(LI->getOperand(0));
                        record.type = 'U';
                        record.statement_th = curr_statement;
                    }

                    if ((LSTarget != NULL) && (LSTarget->getOpcode() == llvm::Instruction::GetElementPtr)) { 
                        const llvm::GetElementPtrInst *GepI = llvm::dyn_cast<llvm::GetElementPtrInst>(LSTarget);

                        if (arrayMaxSize < GepI->getSourceElementType()->getArrayNumElements())
                            arrayMaxSize = GepI->getSourceElementType()->getArrayNumElements();

                        // find the array's name
                        llvm::AllocaInst *AI_arr = llvm::dyn_cast<llvm::AllocaInst>(GepI->getOperand(0));
                        record.arrName = VarNameDict[AI_arr];

                        // find the array's location (line, column) in the source code
                        const llvm::DILocation *Loc = GepI->getDebugLoc();
                        record.lineNum = Loc->getLine();
                        record.columnNum = Loc->getColumn();

                        llvm::SExtInst *SEI = llvm::dyn_cast<llvm::SExtInst>(GepI->getOperand(2));
                        llvm::Instruction *inst = llvm::dyn_cast<llvm::Instruction>(SEI->getOperand(0));

                        // find iAddend
                        if (inst->getOpcode() == llvm::Instruction::Sub) {
                            llvm::ConstantInt *C = llvm::dyn_cast<llvm::ConstantInt>(inst->getOperand(1));
                            record.iAddend = -1 * C->getSExtValue();
                            inst = llvm::dyn_cast<llvm::Instruction>(inst->getOperand(0));
                        } 
                        else if (inst->getOpcode() == llvm::Instruction::Add) {
                            llvm::ConstantInt *C = llvm::dyn_cast<llvm::ConstantInt>(inst->getOperand(1));
                            record.iAddend = +1 * C->getSExtValue();
                            inst = llvm::dyn_cast<llvm::Instruction>(inst->getOperand(0));
                        } 
                        else {
                            record.iAddend = 0;
                        }

                        // find iMultiplier
                        if (inst->getOpcode() == llvm::Instruction::Mul) {
                            llvm::ConstantInt *C = llvm::dyn_cast<llvm::ConstantInt>(inst->getOperand(0));
                            record.iMultiplier = C->getSExtValue(); // if muliplier < 0 ???
                            inst = llvm::dyn_cast<llvm::Instruction>(inst->getOperand(1));
                        } 
                        else {
                            record.iMultiplier = 1;
                        }

                        // find iName
                        llvm::LoadInst *LI = llvm::dyn_cast<llvm::LoadInst>(inst);
                        llvm::AllocaInst *AI_i = llvm::dyn_cast<llvm::AllocaInst>(LI->getOperand(0));
                        record.iName = VarNameDict[AI_i];

                        // find i range (start, stop, step)
                        record.i_start = find_i_start(AI_i, L->getLoopPreheader());
                        record.i_stop = find_i_stop(AI_i, L->getExitingBlock());
                        record.i_step = find_i_step(AI_i, L->getLoopLatch());

                        // push the found record
                        ArrAccessList.push_back(record);
                    } // end of if is a load or store instr with a GEP instr 
                } // end of for each instr 
            } // end of for each BB, end of construct ArrAccessList


            // key: arrName, value: a vector of Arr access (ArrInfo)
            std::map<std::string, std::vector<ArrInfo>> EachArrDict; 
            for (ArrInfo x : ArrAccessList) EachArrDict[x.arrName].push_back(x);

            // construct EachArrTable
            // key: arrName, value: access table
            std::map<std::string, std::vector<std::vector<ArrAccessInfo>>> EachArrTable;
            for (auto x : EachArrDict){ // for each arr (of distinct name)
                std::vector<ArrInfo> arrInfo_list = x.second;
                if(arrInfo_list.size() > 1){ // find dependencies only if we access an array more then one time in a loop...
                    // construct table
                    std::vector<std::vector<ArrAccessInfo>> table(arrayMaxSize);  
                    ArrAccessInfo record;
                    int64_t i_start = arrInfo_list.at(0).i_start; 
                    int64_t i_stop  = arrInfo_list.at(0).i_stop;
                    int64_t i_step  = arrInfo_list.at(0).i_step;

                    for (int64_t i = i_start; i < i_stop; i+=i_step){
                        for (ArrInfo arrInfo : arrInfo_list){
                            int64_t index = arrInfo.iMultiplier * i + arrInfo.iAddend;
                            if (index >= 0 && index < arrayMaxSize){ // filter: index must >= 0
                                record.type = arrInfo.type;
                                record.statement_th = arrInfo.statement_th;
                                record.iName = arrInfo.iName;
                                record.i = i;
                                table.at(index).push_back(record); 
                            }
                        }
                    }

                    EachArrTable[x.first] = table;
                }
            }

            // report dependencies
            report_dependence(EachArrTable, "flow");
            report_dependence(EachArrTable, "anti");
            report_dependence(EachArrTable, "output");

            return false;
        } // end of runOnLoop
    }; // end of struct HW1Pass
} // end of namespace

char HW1Pass::ID = 0;

//register class
static llvm::RegisterPass<HW1Pass> X("hw1", "hw1 PASS, Dependence Analysis");
